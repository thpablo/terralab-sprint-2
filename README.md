# SPRINT 2 - Respostas e Códigos

Este repositorio contém as respostas do estudo dirigido. 

## Objetivos

- [x] Compreender a sintaxe de um arquivo Markdown e a construção de um arquivo README. [Medium](https://raullesteves.medium.com/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8)

- [x] Compreender os comandos e funcionamento de Git, além da criação de um repositório no Gitlab.
- [x] Preparação do ambiente de trabalho para desenvolvimento de React Native e Javascript/Typescript.

## Formatação de texto em Markdown.

_Itálico_
**Negrito**
~~Tachado~~
> A verdadeira motivação vem de realização, desenvolvimento pessoal, satisfação no trabalho e reconhecimento.


## Inserção de um código em Markdown

```
    const args = process.argv.slice(2);
    console.log(parseInt(args[0]) + parseInt(args[1]));
```

## Inserção de Imagens em Markdown

![](/src/doggo.jpg)